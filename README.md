# Welcome Week - Computer Science

| **NAME OF SESSION** | **SHORT DESCRIPTION** | **DATE OF SESSION** | **SESSION START TIME** | **SESSION END TIME** | **Online Link** | **Staff** | **Building and Room Information** |
| --- | --- | --- | --- | --- | --- | --- | --- |
|  |  |  |  |  |  |  |  |
| **Part 1 students** |  |  |  |  |  |  |  |
| Welcome to the School/Department of Computer Science | Head of School/Department Welcome, All Part 1 UG and New PG | 20-Sep | 11:00 | 12:00 | [Click here to join the meeting](https://teams.microsoft.com/l/meetup-join/19%3Ameeting_OTZjNzdhMDUtNTE0Ny00ZDVmLWEyM2QtOGM1ZjQ5YjdhODZj%40thread.v2/0?context=%7B%22Tid%22%3A%224ffa3bc4-ecfc-48c0-9080-f5e43ff90e5f%22%2C%22Oid%22%3A%22a3b6b9be-0fec-4997-9fb1-fc032106f658%22%7D) | [Professor Andrew Carlton- Purez (HoS)](https://research.reading.ac.uk/meteorology/people/andrew-charlton-perez/), [Dr Lily Sun (HoD)](https://www.reading.ac.uk/en/computer-science/staff/dr-lily-sun) |
| Intro to Labs * | Drop in Sessions | 20-Sep | 13:00 | 15:00 |   | [Dr Pat Parslow](https://www.reading.ac.uk/en/computer-science/staff/dr-pat-parslow) and [Dr Elly Liang](https://www.reading.ac.uk/computer-science/staff/dr-huizhi-liang) | G56 Polly Vacher building (38) |
| Intro to Labs * | Drop in Sessions | 20-Sep | 15:00 | 17:00 |   | [Dr Pat Parslow](https://www.reading.ac.uk/en/computer-science/staff/dr-pat-parslow) and [Dr Elly Liang](https://www.reading.ac.uk/computer-science/staff/dr-huizhi-liang) | G56 Polly Vacher building (38) |
| SMPCS whole school transition event | Transitioning to University | 21-Sep | 11:00 | 12:00 | [Click here to join the meeting](https://teams.microsoft.com/l/meetup-join/19%3Ameeting_YmQxYjE2NjgtMmM0Zi00NzQ4LWE4ZDQtODAyYzkxNjVhOGVj%40thread.v2/0?context=%7B%22Tid%22%3A%224ffa3bc4-ecfc-48c0-9080-f5e43ff90e5f%22%2C%22Oid%22%3A%2231d48c03-0e5a-4eae-adf0-11c89cb50ce8%22%7D) | [Dr Calvin Smith (SDTL)](https://www.reading.ac.uk/maths-and-stats/staff/calvin-smith) and others  | HBS G11 (138) |
| Academic Tutor meetings * | First contact | 21-Sep | refer to your emails | refer to your emails  |   | refer to your emails | refer to your emails |
| Programme Welcome (UG - BSc) | How to succeed and thrive | 22-Sep | 10:00 | 11:00 |   | [Dr Lily Sun](https://www.reading.ac.uk/en/computer-science/staff/dr-lily-sun) | JJT Slingo (3) |
| Programme Welcome (PGT - MSc) | How to succeed and thrive | 23-Sep | 10:00 | 11:00 |   | [Dr Carman Lam](https://www.reading.ac.uk/computer-science/staff/dr-carmen-lam) | CHEM LTG (6) |
| SMPCS Marketplace | Drop in marketplace to explore University life | 23-Sep | 11:45 | 12:45 |   | Allison Penn | Chancellors G06 (building near P7) |
| Facilitate your Team work; tools and discussion drop in session * | Drop in sessions | 23-Sep | 10:00 | 12:00 |   | [Dr Pat Parslow](https://www.reading.ac.uk/en/computer-science/staff/dr-pat-parslow) | G56 Polly Vacher (38) |
|  |  |  |  |  |  |  |  |
| Programme Welcome (Non-Lead School, non-CS students) | How to study in joint programmes | 22-Sep | 13:00 | 14:00 |   | [Dr Lily Sun](https://www.reading.ac.uk/en/computer-science/staff/dr-lily-sun) | AGRIC 1L14 (59) |
|  |  |  |  |  |  |  |  |
| **Part 2 students** |  |  |  |  |  |  |  |
| Welcome to campus! | UG Part 2 | 24-Sep | 12:15 | 13:30 |   | [Dr David Ferreira](https://research.reading.ac.uk/meteorology/people/david-ferreira/), [Dr Pat Parslow](https://www.reading.ac.uk/en/computer-science/staff/dr-pat-parslow) | Room Madjeski Theatre in Agric (59) |

## NOTES: 

* Drop in sessions on Monday 20th are designed for you to come and familiarise yourself with our lab PCs; you do not have to attend for the whole session, and do not need to attend both sessions

* Drop in session on Thursday 23rd is a chance to meet other students, staff who are available, and get advice on using available software tools to facilitate group working

* Academic Tutor meetings - please refer to your emails for the date, location and information about who your tutor is.

### Map with numbered buildings


![Map of campus with building numbers](./image_1.png)
[PDF version with key to building numbers)(https://www.reading.ac.uk/web/files/University-of-Reading-map-WhiteknightsA-Z-colour.pdf)

### Schematic of Polly Vacher building and surrounds:
![image.png](./image.png)



For CS UG students only [VM image](https://livereadingac-my.sharepoint.com/:u:/g/personal/sis06pp_reading_ac_uk/ETf7DGn4wdxCtZJB2WnWZVYBp21NpxHyru727rCA4ggByw?e=7gtGUM)
